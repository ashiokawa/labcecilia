package main;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

import upload.UploadServer;

public class Main {

	
	public static final int DEFAULT_SERVER_PORT = 6626;
	public static void main(String[] args) {
		
		if(args.length==0 || args.length > 2){
			System.out.println("Usage: quantashare <TRACKER> [port]");
			System.exit(1);
		}
		if(args.length ==2){
			int port = DEFAULT_SERVER_PORT;
			try {
				port = Integer.parseInt(args[1]);
			} catch (NumberFormatException e) {
				e.printStackTrace();
			}
			if(port>65535){
				System.out.println("Invalid port, defaulting to"+DEFAULT_SERVER_PORT);
			}
		}
		String server = args[0];
		//Update tracker	
		System.out.println("Starting Upload Server");
		UploadServer us = new UploadServer();
		us.run();
		

	}

}
