package upload;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

public class UploadWorker implements Runnable{

	private Socket s;
	private boolean open = true;
	private PrintWriter out;
	private BufferedReader in;
	public UploadWorker(Socket s){
		this.s=s;
		try{
			in = new BufferedReader(new InputStreamReader(s.getInputStream()));
			out = new PrintWriter(s.getOutputStream());
		}
		catch(IOException e){
			e.printStackTrace();
		}	
	}
	
	@Override
	public void run() {
		String received,filename,part = "";
		received = in.readLine();
		String[] msg = received.split(" ");
		if(msg.length!=3){/*malformed message*/}
		else{
			filename = msg[1];
			part = msg[2];
		}
		out.print("Ack");
		received = in.readline();
		if(received.equals("Start")){
			this.sendFile(filename,part);
		}
	}
	private void sendFile(String filename, String part) {
		
	}

	public boolean closed(){
		return !open;
	}
	public void close(){
		open = false;
		try {
			s.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
