package upload;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

public class UploadServer implements Runnable{

	
	private int port;
	private boolean running = true;
	private List<UploadWorker> workers = new ArrayList<UploadWorker>();
	private static final int DEFAULT_PORT = 6626; 
	public UploadServer(){
		this.port = DEFAULT_PORT;
	}
	public UploadServer(int port){
		this.port = port;
	}
	@Override
	public void run() {
		try {
			ServerSocket ss = new ServerSocket(port);
			while(running){
				Socket uploader = ss.accept();
				UploadWorker uw = new UploadWorker(uploader);
				workers.add(uw);
				uw.run();
			}
			for(UploadWorker uw:workers){
				if(!uw.closed()){
					uw.close();
				}
			}
			ss.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
	public void stop(){
		this.running = false;
	}

}
